#pragma once
#include "PlayerInputComponent.h"
#include "GraphicsComponent.h"

struct SDL_Window;

namespace dae
{
	class Minigin
	{
		const int msPerFrame = 16; //16 for 60 fps, 33 for 30 fps		may need tuning for variable time step
		SDL_Window* window{};
	public:
		void Initialize();
		void LoadGame();
		void Cleanup();
		void Run();

		void CreatePlayerObject();

	private:
		void Render() const;

		std::vector<GameObject> m_GameObjects{};
		std::vector<InputComponent> m_InputComponents{};
		std::vector<GraphicsComponent> m_GraphicsComponents{};
	};
}