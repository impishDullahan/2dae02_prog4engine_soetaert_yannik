#pragma once
#include "GameObject.h"
#include <XInput.h>
#include "Commands.h"

namespace dae {

	class InputComponent {

	public:
		InputComponent();
		virtual ~InputComponent();

		virtual void HandleInput(GameObject& go);

	};

}
