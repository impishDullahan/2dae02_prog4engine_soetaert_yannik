#pragma once
#include "Texture2D.h"
#include "Transform.h"
#include "Observer.h"

namespace dae {

	class GraphicsComponent : public Observer
	{
	public:
		GraphicsComponent();
		~GraphicsComponent();

		void Update(float deltaT);
		void Draw() const;

		void SetPosition(const Transform& t);
		void SetPosition(float x, float y);
		void SetTextureID(int id = 0);
		void AddTexture(const std::string& filename);

	private:
		std::vector<std::shared_ptr<Texture2D>> m_pTextures{};
		int m_TextureID{ 0 };
		Transform m_Transform{};
	};

}
