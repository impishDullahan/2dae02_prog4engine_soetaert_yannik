#pragma once
#include <memory>

#include "Transform.h"
#include "Texture2D.h"
#include "SceneObject.h"
#include "GraphicsComponent.h"

#include "States.h"

namespace dae
{

	class GameObject final : public SceneObject
	{
	public:
		GameObject() = default;
		GameObject(GraphicsComponent* graphics);
		virtual ~GameObject();
		GameObject(const GameObject& other);
		GameObject(GameObject&& other);
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

		void Update(float deltaT) override;
		void Render() const override;

		void AddTexture(const std::string& filename);
		void SetPosition(float x, float y);

	private:
		Transform m_Transform;
		std::shared_ptr<Texture2D> m_pTexture;
		GraphicsComponent* m_pGraphics;

		States::State m_State{};
		//void* m_State;
	};
}
