#pragma once
#include <XInput.h>
#include "Singleton.h"
#include "Commands.h"

namespace dae
{
	enum class ControllerButton
	{
		ButtonA,
		ButtonB,
		ButtonX,
		ButtonY
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		bool ProcessInput();
		bool IsPressed(ControllerButton button) const;
	private:
		XINPUT_STATE currentState{};

		std::unique_ptr<Command> m_pButtonA;
		std::unique_ptr<Command> m_pButtonB;
		std::unique_ptr<Command> m_pButtonX;
		std::unique_ptr<Command> m_pButtonY;

		std::unique_ptr<Command> m_pButtonUp;
		std::unique_ptr<Command> m_pButtonDown;
		std::unique_ptr<Command> m_pButtonLeft;
		std::unique_ptr<Command> m_pButtonRight;
		std::unique_ptr<Command> m_pButtonPump;
	};

}
