#include "MiniginPCH.h"
#include "Subject.h"
#include <algorithm>

dae::Subject::Subject()
{
}


dae::Subject::~Subject() {

	for (auto obs : m_pObservers) {

		obs = nullptr;
		delete obs;

	}

}

void dae::Subject::AddObserver(Observer* observer) {

	m_pObservers.push_back(observer);

}

void dae::Subject::RemoveObserver(Observer* observer) {

	std::remove_if(m_pObservers.begin(), m_pObservers.end(), [observer](Observer* obs) {return obs == observer; });

}

void dae::Subject::Notify(const GameObject& go, Events event) {

	for (auto obs : m_pObservers)
		obs->OnNotify(go, event);

}
