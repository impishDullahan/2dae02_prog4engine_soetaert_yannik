#include "MiniginPCH.h"
#include "GraphicsComponent.h"

#include "Renderer.h"
#include "ResourceManager.h"


dae::GraphicsComponent::GraphicsComponent() {



}

dae::GraphicsComponent::~GraphicsComponent() {

}

void dae::GraphicsComponent::Update(float deltaT) {

	deltaT;

}

void dae::GraphicsComponent::Draw() const {

	Renderer::GetInstance().RenderTexture(*m_pTextures[m_TextureID], m_Transform.GetPosition().x, m_Transform.GetPosition().y);

}

void dae::GraphicsComponent::SetPosition(const Transform& t) {

	m_Transform = t;

}

void dae::GraphicsComponent::SetPosition(float x, float y) {

	m_Transform.SetPosition(x, y, 0.f);

}

void dae::GraphicsComponent::SetTextureID(int id) {

	m_TextureID = id;

}

//to be added in an order to match with state enum value (textureID)
void dae::GraphicsComponent::AddTexture(const std::string& filename) {

	m_pTextures.push_back(ResourceManager::GetInstance().LoadTexture(filename));

}
