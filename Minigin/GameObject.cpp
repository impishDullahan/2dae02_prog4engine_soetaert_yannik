#include "MiniginPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"

dae::GameObject::GameObject(GraphicsComponent* graphics) :
	m_pGraphics{ graphics }
{
}

dae::GameObject::~GameObject() {

	m_pGraphics = nullptr;
	delete m_pGraphics;

}

dae::GameObject::GameObject(const GameObject & other) :
	m_pGraphics{ other.m_pGraphics }
{
}

dae::GameObject::GameObject(GameObject && other) :
	m_pGraphics{ other.m_pGraphics }
{
}

void dae::GameObject::Update(float deltaT) {

	m_pGraphics->SetPosition(m_Transform);
	m_pGraphics->SetTextureID(m_State);

	deltaT;

}

void dae::GameObject::Render() const {

	m_pGraphics->Draw();

}

void dae::GameObject::AddTexture(const std::string& filename) {

	m_pGraphics->AddTexture(filename);

}

void dae::GameObject::SetPosition(float x, float y) {

	m_Transform.SetPosition(x, y, 0.0f);

}
