#pragma once
#include "GameObject.h"
#include "Events.h"

namespace dae {

	class Observer {

	public:
		virtual ~Observer() = default;
		virtual void OnNotify(const GameObject& go, Events event) = 0;

	};

}
