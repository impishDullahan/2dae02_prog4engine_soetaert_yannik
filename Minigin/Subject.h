#pragma once
#include "Observer.h"

namespace dae {

	class Subject {

	public:
		Subject();
		~Subject();

		void AddObserver(Observer* observer);
		void RemoveObserver(Observer* observer);

	protected:
		void Notify(const GameObject& go, Events event);

	private:
		std::vector<Observer*> m_pObservers;

	};

}
