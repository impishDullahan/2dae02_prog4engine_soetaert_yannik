#include "MiniginPCH.h"
#include "Minigin.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>
#include "TextObject.h"
#include "GameObject.h"
#include "Scene.h"
#include "States.h"


void dae::Minigin::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		560,
		720,
		SDL_WINDOW_OPENGL
	);
	if (window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(window);
}

/**
 * Code constructing the scene world starts here
 */
void dae::Minigin::CreatePlayerObject() {

	m_InputComponents.push_back(PlayerInputComponent{});
	m_GraphicsComponents.push_back(GraphicsComponent{});
	m_GameObjects.push_back(GameObject{ &m_GraphicsComponents.back() });

	m_GraphicsComponents.back().AddTexture("DigDugBlue.png");

	Transform pos{};
	pos.SetPosition(320, 240, 0);
	m_GraphicsComponents.back().SetPosition(pos);

}

void dae::Minigin::LoadGame()
{
	auto& scene = SceneManager::GetInstance().CreateScene("Demo");

	//add new default component to back of each component list
	//instantiate each game object with references to back of each list
	//add to scene

	m_GraphicsComponents.push_back(GraphicsComponent{});
	m_GameObjects.push_back(&m_GraphicsComponents.back());
	m_GraphicsComponents.back().AddTexture("background.jpg");

	auto go = std::make_shared<GameObject>(m_GameObjects.back());
	scene.Add(go);

	m_GraphicsComponents.push_back(GraphicsComponent{});
	m_GameObjects.push_back(GameObject{ &m_GraphicsComponents.back() });
	m_GraphicsComponents.back().AddTexture("logo.png");
	m_GraphicsComponents.back().SetPosition(216, 180);

	go = std::make_shared<GameObject>(&m_GraphicsComponents.back());
	scene.Add(go);

	auto font = ResourceManager::GetInstance().LoadFont("Lingua.otf", 36);
	auto to = std::make_shared<TextObject>("Programming 4 Assignment", font);
	to->SetPosition(80, 20);
	scene.Add(to);

	//CreatePlayerObject();
}

void dae::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(window);
	window = nullptr;
	SDL_Quit();
}

void dae::Minigin::Run()
{
	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");

	LoadGame();

	{
		//auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		auto& input = InputManager::GetInstance();

		bool doContinue = true;
		auto lastTime = std::chrono::high_resolution_clock::now();
		double lag = 0.0;
		while (doContinue)
		{
			auto currentTime = std::chrono::high_resolution_clock::now();
			float deltaT = std::chrono::duration<float>(currentTime - lastTime).count();
			lastTime = currentTime;
			lag += deltaT;

			doContinue = input.ProcessInput();

			//fixed time step update
			for (lag; lag >= msPerFrame; lag -= msPerFrame)
				sceneManager.Update(deltaT);

			//renderer.Render();		//out of bounds exception
			Render();				//doesnt draw anything
		}
	}

	Cleanup();
}

void dae::Minigin::Render() const {

	for (unsigned int c = 0; c < m_GraphicsComponents.size(); c++)
		m_GraphicsComponents[c].Draw();

}
