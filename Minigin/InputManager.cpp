#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>

bool dae::InputManager::ProcessInput()
{
	ZeroMemory(&currentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &currentState);

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT)
			return false;

		if (e.type == SDL_KEYDOWN) {
			/*
			if (e.type == SDLK_w) 		m_pButtonUp->Execute();
			if (e.type == SDLK_s) 		m_pButtonDown->Execute();
			if (e.type == SDLK_a) 		m_pButtonLeft->Execute();
			if (e.type == SDLK_d) 		m_pButtonRight->Execute();
			*/
		}

		if (e.type == SDL_MOUSEBUTTONDOWN) {
			
			//if (e.type == SDL_BUTTON_LEFT) m_pButtonPump->Execute();

		}
	}

	return true;
}

bool dae::InputManager::IsPressed(ControllerButton button) const
{
	switch (button)
	{
	case ControllerButton::ButtonA:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_A;
	case ControllerButton::ButtonB:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	case ControllerButton::ButtonX:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	case ControllerButton::ButtonY:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	default: return false;
	}
}

