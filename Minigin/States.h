#pragma once

namespace dae {

	class States {

	public:
		enum State {

			DefaultState = 0

		};

	};

	class PlayerState : States {

	public:
		enum State {

			idle = 0,
			jump = 1

		};

	};

}
