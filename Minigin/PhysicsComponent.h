#pragma once
#include "GameObject.h"

namespace dae {

	class PhysicsComponent {

	public:
		PhysicsComponent();
		~PhysicsComponent();

		void Update(float deltaT, GameObject& go);

	};

}
