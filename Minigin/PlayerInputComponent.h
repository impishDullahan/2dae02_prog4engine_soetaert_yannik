#pragma once
#include "InputComponent.h"

namespace dae {

	class PlayerInputComponent : public InputComponent {

	public:
		PlayerInputComponent();
		~PlayerInputComponent();

		void HandleInput(GameObject& go);

	};

}
