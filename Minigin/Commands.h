#pragma once
#include "GameObject.h"

class Command {

public:
	virtual ~Command() {};
	virtual void Execute(dae::GameObject& actor) = 0;

};

class UpCommand : public Command {

public:
	virtual void Exectute(dae::GameObject& actor) { actor; };

};

class DownCommand : public Command {

public:
	virtual void Exectute(dae::GameObject& actor) { actor; };

};

class LeftCommand : public Command {

public:
	virtual void Exectute(dae::GameObject& actor) { actor; };

};

class RightCommand : public Command {

public:
	virtual void Exectute(dae::GameObject& actor) { actor; };

};

class PumpCommand : public Command {

public:
	virtual void Execute(dae::GameObject& actor) { actor; };

};
