Required Patterns:
	Game Loop	x
	Update		x
	Command		
	State		
	Observer	
	Component	c
Threading
Git Repo
all 4 build targets, warning level 4, warnings as errors

void pointer, cast to data type pointer

Data Locality, arrange data for improved performance
	fast cpu, memory still behind, why no idle?
	fetch chunk of memory to be cached for potential future use
	but what if needs memory from another chunk?
	organise to fit as much in a cache line as possible to avoid cache misses
	use for high performance code executed very frequently
	profile and gut data structures after, keep cache-friendly in meantime
	data locality sacrifices flexibility and abstraction (inheritence, interfaces, etc)
	
	Contiguous Arrays, game loops
		Game maintains array of game objects with many small components
		need to U/D each GOComponent
		cycling through each type of component separately results in many misses
		maintain arrays for each component type, no pointer
			update comp1.1 > comp1.2 > etc
			update comp2.1 > comp2.2 > etc
			etc
		GOs point to components in arrays
	Packed Data, particle systems
		for array of GOs that can be inactive, sort array
		all needed GOs at front, no misses, only loop through active portion
		when GO activation is toggle, sort into place
		lost felxibility, tightly bound to manager
		make sure to update pointers of new data location when swapped
		
	Hot/Cold Splitting
		split data members between needed for every frame and only in certain circumstances
		pointer(s) to cold member class(es)
		
	not ideal for polymorphism/inheritence
	use separate arrays for each type
	use collection of pointers in GO to interfaces for polymorphism, not cache-friendly
	
	can use ids to components if not using pointers
	entities can just be IDs if component arrays are in parallel
	
Object Pool, improves memory performance by avoiding constant (de)allocation
	instead of creating/destroying objects, use pool of objects with activity
	size of pool must be tuned
		too small, game will crash
		too large, wasted memory
	
	tune sizes for different scenarios
	kill unimportant objects if new object would be sorely missed
	bump size at runtime if possible
	
	make sure to dereference and deallocate properly, garbage collector will conflict or not touch at all
	
	can keep linked list of inactive objects
	to activate an object, remove and activate head
	
Game Loop, independent from user input and processor speed
	get/process input
	update
	render
	
	fixed time step
		running at processor speed
		
	fixed time step with synchronisation
		sleeps until next frame after finishing calcs
		
	variable time step
		accomodates lag
		accumulates errors easily
		
	fixed update time step, variable rendering
		complex but adaptable
		runs on fast and slow easily
		requires tuning
		
Update, simulates multiple objects each frame
	removing objects during an update cycle can skip updates on other objects
	take into account or defer removals (mark as dead, clean up later)
	trinity with game loop and components for game engines
	
Components, entity spanning different, uncoupled domains
	plug-and-play, reusable code
	memory management and communication more difficult
	
	functions for creating different game objects
	
	GameObject, component bag
	Input, interface
		player input
		demo input
		etc
	Physics, interface
		player physics
		enemy physics
		etc
	Graphics, interface
		player graphics
		particle graphics
		etc
		
	intercomponent communications
		push up into game object
		reference one component in another
		send data to container to be received by components
		
Commands, reified method calls
	Configuring inputs
		Command interface
			jump command
			fire command
		each command object can have a different, programmable trigger and execution
	
	Directions for Actors
		pass actor to execute function
		input handler simply retrieves the command, doesnt execute
		enemy input handler simply emits commands, doesnt get player input
		
Singleton
	ensures there is only a single instance of a game object
	provides gloabal access
	use sparingly if at all
		pass
		get from base class
		initilise in an already global class (game world)
	concrete class linked to every reference
	
Service Locator, provide global access without coupling
	effectively a phone book listing, can be called from anywhere but doesnt reference anywhere in particular
	avoids linking to concrete class like singleton
	use sparingly where passing objects would be too much
	use on singular classes needed almost everywhere
	service still has to be located, a call can missed
	code must work in any circumstance, doesnt know whats accessing it
	must catch null if service hasnt been provided yet, return null objects
	
	Logging Decorator
		logging service for particular domains
		
	can define in a base class if service is only used in 1 domains
	basically Unity's GetComponent<>()
	
Observer
	practically universal
	lets piece of code announce something, cry into the wind
	whats done with notifs completely separate from trigger code
	
	The Observer
		interface for concrete class
	
	The Subject
		hold list of observers
		observers can be added/removed from outside, public
		provides code to notify the observers, protected
		systems inherit from subject
		linked list to observers to improve performance over a list?
			pool of oberservers to subjects?
	
	remember to handle the removal of an observer/destructor in the other
	bare minimum of communication between very different domains

Double Buffer, makes sequential operations appear simultaneously
	allows display driver to see all pixels that need to be drawn at once
	draws one screen whilst another is being shown, then swaps
	class encapsultes two instances of a buffer
		info read from currentBuffer
		info written to next buffer
		then swapped
	not just for graphics
	repoint swap
		fast, inaccesible from elsewhere
	copy swap
		slow, can be accessed from elsewhere

State
	FSMs
	simple enum and switch block enough
	
Bytecode
	hard code to instructions, more flexible
	separate from exe
	
	
	
GameObject collection of pointers to components saved in arrays in minigin